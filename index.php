<?php
/*
Plugin Name: Basit - Admin
Description: Sample Hello World Plugin using WordPress Fuel
Version: 1.0
*/
require_once "mvc/init.php";
class basit extends absMVC_Plugin
{ 
	protected $_plugin_slug = 'helloworld'; protected function _init(){	
		//nothing to init 
	} 
	public function registerAdminMenu() { 
		$role = 'administrator'; 
		add_menu_page('Hello World', 'Hello World',$role, __FILE__); 
		add_submenu_page(__FILE__, 'Welcome', 'Welcome',$role, $this->_getAdminPageSlug('home'), array($this , 'handleAdminMenu') ); 
		//output: http://yourdomain.com/wp-admin/admin.php?page=helloworld_home 
	}
}
new basit(__FILE__);
?>